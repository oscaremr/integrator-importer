package org.oscarehr.integrator.importer;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.oscarehr.PMmodule.caisi_integrator.ByteWrapper;
import org.oscarehr.PMmodule.caisi_integrator.DeleteCachedDemographicIssuesWrapper;
import org.oscarehr.PMmodule.caisi_integrator.DeleteCachedDemographicPreventionsWrapper;
import org.oscarehr.PMmodule.caisi_integrator.IntegratorFileFooter;
import org.oscarehr.PMmodule.caisi_integrator.IntegratorFileHeader;
import org.oscarehr.PMmodule.caisi_integrator.ProgramDeleteIdWrapper;
import org.oscarehr.caisi_integrator.ws.CachedAdmission;
import org.oscarehr.caisi_integrator.ws.CachedAppointment;
import org.oscarehr.caisi_integrator.ws.CachedBillingOnItem;
import org.oscarehr.caisi_integrator.ws.CachedDemographicAllergy;
import org.oscarehr.caisi_integrator.ws.CachedDemographicDocument;
import org.oscarehr.caisi_integrator.ws.CachedDemographicDrug;
import org.oscarehr.caisi_integrator.ws.CachedDemographicForm;
import org.oscarehr.caisi_integrator.ws.CachedDemographicIssue;
import org.oscarehr.caisi_integrator.ws.CachedDemographicLabResult;
import org.oscarehr.caisi_integrator.ws.CachedDemographicNote;
import org.oscarehr.caisi_integrator.ws.CachedDemographicPrevention;
import org.oscarehr.caisi_integrator.ws.CachedDxresearch;
import org.oscarehr.caisi_integrator.ws.CachedEformData;
import org.oscarehr.caisi_integrator.ws.CachedEformValue;
import org.oscarehr.caisi_integrator.ws.CachedFacility;
import org.oscarehr.caisi_integrator.ws.CachedMeasurement;
import org.oscarehr.caisi_integrator.ws.CachedMeasurementExt;
import org.oscarehr.caisi_integrator.ws.CachedMeasurementMap;
import org.oscarehr.caisi_integrator.ws.CachedMeasurementType;
import org.oscarehr.caisi_integrator.ws.CachedProgram;
import org.oscarehr.caisi_integrator.ws.DemographicTransfer;
import org.oscarehr.caisi_integrator.ws.FacilityWs;
import org.oscarehr.caisi_integrator.ws.ImportLog;
import org.oscarehr.caisi_integrator.ws.ProviderTransfer;
import org.oscarehr.caisi_integrator.ws.SetConsentTransfer;

public class ImportFile {

	static Logger logger = Logger.getLogger(ImportFile.class);
	static Properties properties = null;

	
	private CachedDemographicDocument lastCachedDemographicDocument = null ;
	private IntegratorFileHeader header = null;
	private IntegratorFileFooter footer = null;
	
	private String zipFilename = null;
	private String zipFileChecksum = null;
	private ImportLog importLog = null;
	
	public ImportFile() throws IOException {
		this("importer.cnf");
	}
	
	public ImportFile(String propertyFilename) throws IOException {
		getProperties();
		WSUtils.init(getProperty("baseUrl"), getProperty("username"), getProperty("password"),
				getProperty("providerNo"));
	}

	public static Properties getProperties() throws IOException {
		if (properties == null) {
			Properties temp = new Properties();
			temp.load(ClassLoader.getSystemResourceAsStream("importer.cnf"));
			properties = temp;
		}
		return properties;
	}

	public static String getProperty(String key) {
		return properties.getProperty(key);
	}

	private static String getFileChecksum(MessageDigest digest, File file) throws IOException
	{
	    //Get file input stream for reading the file content
	    FileInputStream fis = new FileInputStream(file);
	     
	    //Create byte array to read data in chunks
	    byte[] byteArray = new byte[1024];
	    int bytesCount = 0; 
	      
	    //Read file data and update in message digest
	    while ((bytesCount = fis.read(byteArray)) != -1) {
	        digest.update(byteArray, 0, bytesCount);
	    };
	     
	    //close the stream; We don't need it now.
	    fis.close();
	     
	    //Get the hash's bytes
	    byte[] bytes = digest.digest();
	     
	    //This bytes[] has bytes in decimal format;
	    //Convert it to hexadecimal format
	    StringBuilder sb = new StringBuilder();
	    for(int i=0; i< bytes.length ;i++)
	    {
	        sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
	    }
	     
	    //return complete hash
	   return sb.toString();
	}
	public boolean importZipFile(File file) throws IOException,NoSuchAlgorithmException {
		this.zipFilename = file.getAbsolutePath();
	
		MessageDigest md5Digest = MessageDigest.getInstance("MD5");
		this.zipFileChecksum = getFileChecksum(md5Digest, file);
			
		List<String> files = new ArrayList<String>();
		
		ZipInputStream in = new ZipInputStream(new FileInputStream(file));
		ZipEntry entry = in.getNextEntry();
		while (entry != null) {
			byte[] buf = new byte[1024];
			int len;

			String entryName = entry.getName();
			String fileName = getProperty("workingDir") + File.separator + entryName;
			OutputStream out = new FileOutputStream(fileName);
			while ((len = in.read(buf)) > 0)
				out.write(buf, 0, len);
			out.close();

			files.add(fileName);
			entry = in.getNextEntry();
		}
		
		IOUtils.closeQuietly(in);
		
		//now process the files in order
		Collections.sort(files );
		boolean error = false;
		for(String f:files) {
			if(!importFile(new File(f))) {
				return false;
			}
		}
		
		return true;
	}

	public boolean importFile(File file) {

		if (!file.exists()) {
			logger.error("File does not exist: " + file.getName());
			return false;
		}

		// this could actually be in the header of the file. but this is
		// probably more secure. You'd need a config per client though
		

		ObjectInputStream in = null;
		try {
			in = new ObjectInputStream(new FileInputStream(file));

			int count = 0;
			while (true) {
				Object obj = null;

				try {
					obj = in.readObject();
					processObject(obj);
				} catch (EOFException e) {
					break;
				}
				count++;
			}
			
			WSUtils.getFacilityWs().updateMyFacilityLastUpdateDate(toCalendar(header.getDate()));

			
			logger.info("done reading file. Found " + count + " objects");
		} catch (IOException e) {
			logger.error("Error reading file " + file.getName(), e);
			if(importLog!=null)WSUtils.getFacilityWs().errorImportLog(importLog.getId().intValue());
			return false;
		} catch (ClassNotFoundException e) {
			logger.error("System Error", e);
			if(importLog!=null)WSUtils.getFacilityWs().errorImportLog(importLog.getId().intValue());
			return false;
		} catch (Exception e) {
			logger.error("System Error", e);
			if(importLog!=null)WSUtils.getFacilityWs().errorImportLog(importLog.getId().intValue());
			return false;

		} finally {
			IOUtils.closeQuietly(in);
		}

		return true;
	}

    public static GregorianCalendar toCalendar(Date date)
    {
            if (date==null) return(null);

            GregorianCalendar cal=new GregorianCalendar();
            cal.setTime(date);
            return(cal);
    }

    
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void processObject(Object obj) throws Exception {
		if (obj instanceof CachedFacility) {
			processCachedFacility((CachedFacility) obj);
		} else if (obj instanceof IntegratorFileHeader) {
			processIntegratorFileHeader((IntegratorFileHeader) obj);
		} else if (obj instanceof IntegratorFileFooter) {
			processIntegratorFileFooter((IntegratorFileFooter) obj);
		} else if (obj instanceof DemographicTransfer) {
			processDemographicTransfer((DemographicTransfer) obj);
		} else if (obj instanceof SetConsentTransfer) {
			processSetConsentTransfer((SetConsentTransfer) obj);
		} else if (obj instanceof CachedDemographicDocument) {
			processCachedDemographicDocument((CachedDemographicDocument) obj);
		} else if (obj instanceof ByteWrapper) {
			processByteWrapper((ByteWrapper) obj);
		} else if (obj instanceof CachedDemographicForm) {
			processCachedDemographicForm((CachedDemographicForm) obj);
		} else if (obj instanceof CachedDemographicLabResult) {
			processCachedDemographicLabResult((CachedDemographicLabResult) obj);
		} else if (obj instanceof ProgramDeleteIdWrapper) {
			processProgramDeleteIdWrapper((ProgramDeleteIdWrapper) obj);
		} else if (obj instanceof DeleteCachedDemographicIssuesWrapper) {
			processDeleteCachedDemographicIssuesWrapper((DeleteCachedDemographicIssuesWrapper) obj);
		} else if (obj instanceof DeleteCachedDemographicPreventionsWrapper) {
			processDeleteCachedDemographicPreventionsWrapper((DeleteCachedDemographicPreventionsWrapper) obj);
		} else if (obj instanceof ArrayList) {
			ArrayList list = (ArrayList) obj;
			list.get(0).getClass();
			if (list.get(0).getClass() == ProviderTransfer.class) {
				processProviderTransfer(list);
			} else if (list.get(0).getClass() == CachedProgram.class) {
				processCachedProgram(list);
			} else if (list.get(0).getClass() == CachedDemographicIssue.class) {
				processCachedDemographicIssue(list);
			} else if (list.get(0).getClass() == CachedDemographicDrug.class) {
				processCachedDemographicDrug(list);
			} else if (list.get(0).getClass() == CachedDemographicPrevention.class) {
				processCachedDemographicPrevention(list);
			} else if (list.get(0).getClass() == CachedDemographicNote.class) {
				processCachedDemographicNote(list);
			} else if (list.get(0).getClass() == CachedAppointment.class) {
				processCachedAppointment(list);
			} else if (list.get(0).getClass() == CachedAdmission.class) {
				processCachedAdmission(list);
			} else if (list.get(0).getClass() == CachedMeasurement.class) {
				processCachedMeasurement(list);
			} else if (list.get(0).getClass() == CachedMeasurementExt.class) {
				processCachedMeasurementExt(list);
			} else if (list.get(0).getClass() == CachedMeasurementType.class) {
				processCachedMeasurementType(list);
			} else if (list.get(0).getClass() == CachedMeasurementMap.class) {
				processCachedMeasurementMap(list);
			} else if (list.get(0).getClass() == CachedDxresearch.class) {
				processCachedDxresearch(list);
			} else if (list.get(0).getClass() == CachedBillingOnItem.class) {
				processCachedBillingOnItem(list);
			} else if (list.get(0).getClass() == CachedEformData.class) {
				processCachedEformData(list);
			} else if (list.get(0).getClass() == CachedEformValue.class) {
				processCachedEformValue(list);
			} else if (list.get(0).getClass() == CachedDemographicAllergy.class) {
				processCachedDemographicAllergy(list);
			} else {
				logger.error("Unknown List Type Found! - " + list.get(0).getClass());
				throw new RuntimeException("System Error");
			}
		} else {
			logger.error("Uknown Type Found! - " + obj.getClass().getName());
			throw new RuntimeException("SystemError");
		}
	}

	private void processCachedFacility(CachedFacility cachedFacility) {
		logger.info("Found a CachedFacility");

		FacilityWs service = WSUtils.getFacilityWs();
		// CachedFacility currentlyCachedFacility = service.getMyFacility();
		service.setMyFacility(cachedFacility);
	}

	private void processSetConsentTransfer(SetConsentTransfer setConsentTransfer) {
		logger.info("Found list of SetConsentTransfer");
	}

	private void processProviderTransfer(ArrayList<ProviderTransfer> providerTransfer) {
		logger.info("Found list of ProviderTransfer");
		WSUtils.getProviderWs().setCachedProviders(providerTransfer);
	}

	private void processCachedProgram(ArrayList<CachedProgram> cachedProgram) {
		logger.info("Found list of CachedProgram");
		WSUtils.getProgramWs().setCachedPrograms(cachedProgram);
	}

	private void processCachedDemographicIssue(ArrayList<CachedDemographicIssue> cachedDemographicIssue) {
		logger.info("Found list of CachedDemographicIssue");
		WSUtils.getDemographicWs().setCachedDemographicIssues(cachedDemographicIssue);
	}

	private void processCachedDemographicPrevention(
			ArrayList<CachedDemographicPrevention> cachedDemographicPrevention) {
		logger.info("Found list of CachedDemographicPrevention");
		WSUtils.getDemographicWs().setCachedDemographicPreventions(cachedDemographicPrevention);
	}

	private void processCachedDemographicNote(ArrayList<CachedDemographicNote> cachedDemographicNote) {
		logger.info("Found list of CachedDemographicNote");
		WSUtils.getDemographicWs().setCachedDemographicNotes(cachedDemographicNote);

	}

	private void processCachedDemographicDrug(ArrayList<CachedDemographicDrug> cachedDemographicDrug) {
		logger.info("Found list of CachedDemographicDrug");
		WSUtils.getDemographicWs().setCachedDemographicDrugs(cachedDemographicDrug);
	}

	private void processCachedAppointment(ArrayList<CachedAppointment> cachedAppointment) {
		logger.info("Found list of CachedAppointment");
		WSUtils.getDemographicWs().setCachedAppointments(cachedAppointment);
	}

	private void processCachedAdmission(ArrayList<CachedAdmission> cachedAdmission) {
		logger.info("Found list of CachedAdmission");
		WSUtils.getDemographicWs().setCachedAdmissions(cachedAdmission);
	}

	private void processCachedMeasurement(ArrayList<CachedMeasurement> cachedMeasurement) {
		logger.info("Found list of CachedMeasurement");
		WSUtils.getDemographicWs().setCachedMeasurements(cachedMeasurement);
	}

	private void processCachedMeasurementExt(ArrayList<CachedMeasurementExt> cachedMeasurementExt) {
		logger.info("Found list of CachedMeasurementExt");
		WSUtils.getDemographicWs().setCachedMeasurementExts(cachedMeasurementExt);
	}

	private void processCachedMeasurementType(ArrayList<CachedMeasurementType> cachedMeasurementType) {
		logger.info("Found list of CachedMeasurementType");
		WSUtils.getDemographicWs().setCachedMeasurementTypes(cachedMeasurementType);
	}

	private void processCachedMeasurementMap(ArrayList<CachedMeasurementMap> cachedMeasurementMap) {
		logger.info("Found list of CachedMeasurementMap");
		WSUtils.getDemographicWs().setCachedMeasurementMaps(cachedMeasurementMap);
	}

	private void processCachedDxresearch(ArrayList<CachedDxresearch> cachedDxresearch) {
		logger.info("Found list of CachedDxresearch");
		WSUtils.getDemographicWs().setCachedDxresearch(cachedDxresearch);
	}

	private void processCachedBillingOnItem(ArrayList<CachedBillingOnItem> cachedBillingOnItem) {
		logger.info("Found list of CachedBillingOnItem");
		WSUtils.getDemographicWs().setCachedBillingOnItem(cachedBillingOnItem);
	}

	private void processCachedEformData(ArrayList<CachedEformData> cachedEformData) {
		logger.info("Found list of CachedEformData");
		WSUtils.getDemographicWs().setCachedEformData(cachedEformData);
	}

	private void processCachedEformValue(ArrayList<CachedEformValue> cachedEformValue) {
		logger.info("Found list of CachedEformValue");
		WSUtils.getDemographicWs().setCachedEformValues(cachedEformValue);
	}

	private void processCachedDemographicAllergy(ArrayList<CachedDemographicAllergy> cachedDemographicAllergy) {
		logger.info("Found list of CachedDemographicAllergy");
		WSUtils.getDemographicWs().setCachedDemographicAllergies(cachedDemographicAllergy);
	}

	private void processCachedDemographicDocument(CachedDemographicDocument cachedDemographicDocument) {
		logger.info("Found a CachedDemographicDocument");
		this.lastCachedDemographicDocument = cachedDemographicDocument;
	}

	private void processByteWrapper(ByteWrapper byteWrapper) {
		logger.info("Found a ByteWrapper");
		if(lastCachedDemographicDocument != null) {
			WSUtils.getDemographicWs().addCachedDemographicDocumentAndContents(lastCachedDemographicDocument,byteWrapper.getData());
		}
		lastCachedDemographicDocument = null;
	}

	private void processCachedDemographicForm(CachedDemographicForm cachedDemographicForm) {
		logger.info("Found a CachedDemographicForm");
		WSUtils.getDemographicWs().addCachedDemographicForm(cachedDemographicForm);
	}

	private void processCachedDemographicLabResult(CachedDemographicLabResult cachedDemographicLabResult) {
		logger.info("Found a CachedDemographicLabResult");
		WSUtils.getDemographicWs().addCachedDemographicLabResult(cachedDemographicLabResult);
	}

	private void processDemographicTransfer(DemographicTransfer demographicTransfer) {
		logger.info("Found a DemographicTransfer");
		WSUtils.getDemographicWs().setLastPushDate(demographicTransfer.getCaisiDemographicId());
		
		WSUtils.getDemographicWs().setDemographic(demographicTransfer);
	}

	private void processProgramDeleteIdWrapper(ProgramDeleteIdWrapper programDeleteIdWrapper) {
		logger.info("Found a ProgramDeleteIdWrapper");
		WSUtils.getProgramWs().deleteCachedProgramsMissingFromList(programDeleteIdWrapper.getIds());
	}
	private void processDeleteCachedDemographicIssuesWrapper(DeleteCachedDemographicIssuesWrapper deleteCachedDemographicIssuesWrapper) {
		logger.info("Found a DeleteCachedDemographicIssuesWrapper");
		WSUtils.getDemographicWs().deleteCachedDemographicIssues(deleteCachedDemographicIssuesWrapper.getDemographicNo(),
				deleteCachedDemographicIssuesWrapper.getKeys());
	}
	private void processDeleteCachedDemographicPreventionsWrapper(DeleteCachedDemographicPreventionsWrapper deleteCachedDemographicPreventionsWrapper) {
		logger.info("Found a DeleteCachedDemographicPreventionsWrapper");
		WSUtils.getDemographicWs().deleteCachedDemographicPreventions(deleteCachedDemographicPreventionsWrapper.getDemographicNo(),
				deleteCachedDemographicPreventionsWrapper.getNonDeletedIds());
	}
	
	private void processIntegratorFileHeader(IntegratorFileHeader integratorFileHeader) {
		logger.info("Found a IntegratorFileHeader");
		this.header = integratorFileHeader;
		
		if(integratorFileHeader.getVersion() != IntegratorFileHeader.VERSION) {
			throw new RuntimeException("Wrong version of file! Make sure the importer and OSCAR are the right versions");
		}
		
		//can we continue?
		String validation = WSUtils.getFacilityWs().canProcessFile(zipFilename.substring(zipFilename.lastIndexOf("/")+1), zipFileChecksum,toCalendar(header.getLastDate()), toCalendar(header.getDate()),header.getDependsOn());
	
		if("OK".equals(validation)) {
			importLog = WSUtils.getFacilityWs().createImportLog(zipFilename.substring(zipFilename.lastIndexOf("/")+1), zipFileChecksum,toCalendar(header.getLastDate()), toCalendar(header.getDate()),header.getDependsOn());
		} else if("INTEGRATOR_HAS_NEWER_DATA".equals(validation)) {
			importLog = WSUtils.getFacilityWs().createImportLogWithStatus(zipFilename.substring(zipFilename.lastIndexOf("/")+1), zipFileChecksum,toCalendar(header.getLastDate()), toCalendar(header.getDate()),header.getDependsOn(),"ERROR");
			throw new RuntimeException("Integrator has newer data, aborting");
		} else if("DEPENDENCY_NOT_MET".equals(validation)) {
			importLog = WSUtils.getFacilityWs().createImportLogWithStatus(zipFilename.substring(zipFilename.lastIndexOf("/")+1), zipFileChecksum,toCalendar(header.getLastDate()), toCalendar(header.getDate()),header.getDependsOn(),"ERROR");
			throw new RuntimeException("Integrator does not have dependencies met ("+integratorFileHeader.getDependsOn()+")");
		} else if("ALREADY_IMPORTED".equals(validation)) {
			importLog = WSUtils.getFacilityWs().createImportLogWithStatus(zipFilename.substring(zipFilename.lastIndexOf("/")+1), zipFileChecksum,toCalendar(header.getLastDate()), toCalendar(header.getDate()),header.getDependsOn(),"ERROR");
			throw new RuntimeException("Integrator already has this file");
		}

	}
	
	private void processIntegratorFileFooter(IntegratorFileFooter integratorFileFooter) {
		logger.info("Found a IntegratorFileFooter");
		this.footer = integratorFileFooter;
		WSUtils.getFacilityWs().completeImportLog(importLog.getId().intValue());
	}
	
	public static void main(String[] args) throws Exception {
		ImportFile program = null;
		
		if(args.length == 1) {
			program = new ImportFile();
		} else if(args.length == 2) {
			program  = new ImportFile(args[1]);
		} else {
			logger.error("Send proper arguments. Filename <config>");
			return;
		}

		
		program.importZipFile(new File(args[0]));

	}

}
