package org.oscarehr.integrator.importer;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.oscarehr.caisi_integrator.ws.DemographicWs;
import org.oscarehr.caisi_integrator.ws.DemographicWsService;
import org.oscarehr.caisi_integrator.ws.FacilityWs;
import org.oscarehr.caisi_integrator.ws.FacilityWsService;
import org.oscarehr.caisi_integrator.ws.ProgramWs;
import org.oscarehr.caisi_integrator.ws.ProgramWsService;
import org.oscarehr.caisi_integrator.ws.ProviderWs;
import org.oscarehr.caisi_integrator.ws.ProviderWsService;

public class WSUtils {

	private static String username;
	private static String password;
	private static String providerNo;
	private static String baseUrl;
	
	static FacilityWs facilityWs = null;
	static ProgramWs programWs = null;
	static DemographicWs demographicWs = null;
	static ProviderWs providerWs = null;
	
	
	
	static void init(String baseUrl, String username, String password, String providerNo) {
		WSUtils.baseUrl = baseUrl;
		WSUtils.username = username;
		WSUtils.password = password;
		WSUtils.providerNo = providerNo;
		
		facilityWs  = getFacilityWs();
		programWs = getProgramWs();
		demographicWs = getDemographicWs();
		providerWs = getProviderWs();
		
	}

	private static URL buildURL(String baseUrl, String servicePoint) throws MalformedURLException {
		return (new URL(baseUrl + '/' + servicePoint + "?wsdl"));
	}

	private static void addAuthenticationInterceptor(Object wsPort) {
		Client cxfClient = ClientProxy.getClient(wsPort);
		
		cxfClient.getOutInterceptors().add(new AuthenticationOutWSS4JInterceptorForIntegrator(
				username, password, providerNo));
	}

	public static FacilityWs getFacilityWs()  {
		if(facilityWs != null) return facilityWs;
		try {
			FacilityWsService service = new FacilityWsService(buildURL(baseUrl, "FacilityService"));
			FacilityWs port = service.getFacilityWsPort();
	
			CxfClientUtilsOld.configureClientConnection(port);
			addAuthenticationInterceptor(port);
			return port;
		}catch(MalformedURLException e) {
			return null;
		}
	}
	
	public static ProviderWs getProviderWs()  {
		if(providerWs != null) return providerWs;
		
		try {
			ProviderWsService service = new  ProviderWsService(buildURL(baseUrl, "ProviderService"));
			ProviderWs port = service.getProviderWsPort();
	
			CxfClientUtilsOld.configureClientConnection(port);
			addAuthenticationInterceptor(port);
			return port;
		}catch(MalformedURLException e) {
			return null;
		}
	}
	
	public static ProgramWs getProgramWs()  {
		if(programWs != null) return programWs;
		try {
			ProgramWsService service = new  ProgramWsService(buildURL(baseUrl, "ProgramService"));
			ProgramWs port = service.getProgramWsPort();
	
			CxfClientUtilsOld.configureClientConnection(port);
			addAuthenticationInterceptor(port);
			return port;
		}catch(MalformedURLException e) {
			return null;
		}
	}
	
	public static DemographicWs getDemographicWs()  {
		if(demographicWs != null) return demographicWs;
		
		try {
			DemographicWsService service = new  DemographicWsService(buildURL(baseUrl, "DemographicService"));
			DemographicWs port = service.getDemographicWsPort();
	
			CxfClientUtilsOld.configureClientConnection(port);
			addAuthenticationInterceptor(port);
			return port;
		}catch(MalformedURLException e) {
			return null;
		}
	}
}
