package org.oscarehr.integrator.importer;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

public class ImportFileWatcher {

	static Logger logger = Logger.getLogger(ImportFile.class);
	
	Properties properties = null;
	String propertyFilename;
	
	public ImportFileWatcher() throws IOException {
		this("importer.cnf");
	}
	
	public ImportFileWatcher(String propertyFilename) throws IOException {
		this.propertyFilename = propertyFilename;
		getProperties();
	}

	public Properties getProperties() throws IOException {
		if (properties == null) {
			Properties temp = new Properties();
			temp.load(ClassLoader.getSystemResourceAsStream(propertyFilename));
			properties = temp;
		}
		return properties;
	}

	public String getProperty(String key) {
		return properties.getProperty(key);
	}

	
	
	
	public void run() {
		
		String scanDirectory = getProperty("scanDirectory");
		if(scanDirectory == null) {
			logger.error("must set scanDirectory in the cnf file");
			return;
		}
		File dir = new File(scanDirectory);
		if(!dir.exists() || !dir.isDirectory()) {
			logger.error("scanDirectory must exist and be a writeable directory");
			return;
		}
		
		String archiveDirectory = getProperty("archiveDirectory");
		if(archiveDirectory == null) {
			logger.error("must set archiveDirectory in the cnf file");
			return;
		}
		File archiveDir = new File(archiveDirectory);
		if(!archiveDir.exists() || !archiveDir.isDirectory()) {
			logger.error("archiveDirectory must exist and be a writeable directory");
			return;
		}
		

		while(true) {
			
			logger.info("scanning...");
			
			File[] files = dir.listFiles(new FilenameFilter() {
				public boolean accept(File dir, String name) {
					if(name.startsWith("IntegratorPush") && name.endsWith(".zip"))
						return true;
					return false;
				}
			});
			
			List<File> fileList = Arrays.asList(files);
			
			Collections.sort(fileList, new Comparator<File>() {
				public int compare(File a, File b) {
					return a.getName().compareTo(b.getName());
				}
			});
			
			try {
				inner: for(File file:fileList) {
					if(!process(file, archiveDir)) {
						break inner;
					}
				}
			}catch(Exception e) {
				logger.error("Error processing file, aborting this run.");
			}
			
			try {
				Thread.sleep(60000);
			}catch(InterruptedException e) {
				logger.error("Error",e);
			}
		}
	}
	
	public boolean process(File file, File archiveDir) throws IOException,NoSuchAlgorithmException {
		logger.info("processing " + file.getName());
		
		ImportFile importer = new ImportFile(this.propertyFilename);
		
		boolean result =  importer.importZipFile(file);
		
		if(result) {
			file.renameTo(new File(archiveDir.getAbsolutePath(),file.getName()));
		}
		
		return result;
	}
	
	public static void main(String args[]) throws Exception {
		ImportFileWatcher program = new ImportFileWatcher();
		program.run();
	}
}
