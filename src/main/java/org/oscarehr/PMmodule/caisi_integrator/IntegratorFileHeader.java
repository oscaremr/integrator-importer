package org.oscarehr.PMmodule.caisi_integrator;

import java.io.Serializable;
import java.util.Date;

public class IntegratorFileHeader  implements Serializable{
	
	private Date lastDate;
	
	private Date date;

	private String dependsOn;
	
	public static final int VERSION = 1;
	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getLastDate() {
		return lastDate;
	}

	public void setLastDate(Date lastDate) {
		this.lastDate = lastDate;
	}

	public String getDependsOn() {
		return dependsOn;
	}

	public void setDependsOn(String dependsOn) {
		this.dependsOn = dependsOn;
	}
	
	public int getVersion() {
		return VERSION;
	}
}
